#ndbot - A Narratice Dice Telegram Bot
This is a Telegram bot implemented in Go. To use the bot you need to generate a Telegram bot API key. Add the API token to a configuration file (see below).

If you'd like a commandline version, have a look at the [ndclient](https://bitbucket.org/HKiOnline/ndclient).

## Available commands
- /roll <dice parameters>
- /help

To roll a dice, you must provide the dice and the number of the dice. Dice types are shortened to single letters for brevity. See below.  
```
Dice types:
a - Ability dice
p - Proficiency dice
b - Boost dice
d - Difficulty dice
c - Challenge dice
s - Setback dice
f - Force dice
% - Percentile dice

Usage:
nt - n is the number of dice, t is the type of the dice
Example: /roll 2a 1p 3d
```
## Installation
You can install the bot by standard Go-way or you can download Debian-package from the download section. The deb contains pre-built binary for 64-bit Debian Linux and Debian-like operating systems (read Ubuntu).

## Configuration
At the moment the configuration is very simple. Just create a simple text file with the following content. Remember to add your actual API token inside the quotes.

```
{
  "apitoken":"ADD API TOKEN HERE"
}
```

When you run the bot, add the file path of the configuration file as parameter. For example:

```
ndbot /path/to/the/config.file
```
