package main

import (
	"bytes"
)

func help() string {

	var helpText bytes.Buffer

	helpText.WriteString("Dice types: \n")
	helpText.WriteString(ABILITY_DTYPE + " - Ability dice\n")
	helpText.WriteString(PROFICIENCY_DTYPE + " - Proficiency dice\n")
	helpText.WriteString(BOOST_DTYPE + " - Boost dice\n")
	helpText.WriteString(DIFFICULTY_DTYPE + " - Difficulty dice\n")
	helpText.WriteString(CHALLENGE_DTYPE + " - Challenge dice\n")
	helpText.WriteString(SETBACK_DTYPE + " - Setback dice\n")
	helpText.WriteString(FORCE_DTYPE + " - Force dice\n")
	helpText.WriteString(PERCETILE_DTYPE + " - Percentile dice\n")
	helpText.WriteString("\n")
	helpText.WriteString("Usage: \n")
	helpText.WriteString("nt - n is the number of dice, t is the type of the dice \n")
	helpText.WriteString("Example: /roll 2a 1p 3d\n")

	return helpText.String()
}
