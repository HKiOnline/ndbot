package main

import (
	"log"
	"strings"
)

const (
	HELP_COMMAND = "/help"
	ROLL_COMMAND = "/roll"
)

func messageProcessor() {

	for message := range bot.Messages {

		log.Println("Narrative Dice Telegram Bot (ndbot) Received a Message: " + message.Text)

		if message.Text == HELP_COMMAND {
			bot.SendMessage(message.Chat, help(), nil)
		}

		if strings.HasPrefix(message.Text, ROLL_COMMAND) {
			result := rollCommand(getParameters(ROLL_COMMAND, message.Text))
			log.Println(result)
			bot.SendMessage(message.Chat, result, nil)
		}

	}

	log.Println("Stopping Narrative Dice Telegram Bot (ndbot)")
}

func getParameters(command string, message string) string {
	command_index := strings.LastIndex(message, command) + len(command)
	message_length := len(message)
	return strings.Trim(message[command_index:message_length], " ")
}
