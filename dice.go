package main

import (
	dice "bitbucket.org/HKiOnline/ndlib"
	"log"
	"strconv"
	"strings"
	"unicode"
)

const (
	ABILITY_DTYPE     = "a"
	PROFICIENCY_DTYPE = "p"
	BOOST_DTYPE       = "b"
	DIFFICULTY_DTYPE  = "d"
	CHALLENGE_DTYPE   = "c"
	SETBACK_DTYPE     = "s"
	FORCE_DTYPE       = "f"
	PERCETILE_DTYPE   = "%"
)

func rollCommand(rawDicepool string) string {
	result := dice.Roll(buildDicepool(parseParameters(rawDicepool)))
	return dice.TextSummary(result)
}

func parseParameters(rawDicepool string) []string {

	rawDicepoolParams := strings.Split(strings.Trim(rawDicepool, " "), " ")

	var dicepoolParams []string

	for _, diceParam := range rawDicepoolParams {

		diceParam = stripSpaces(diceParam)

		if diceParam != "" {
			log.Println("[" + stripSpaces(diceParam) + "]")
			dicepoolParams = append(dicepoolParams, diceParam)
		}

	}

	return dicepoolParams
}

func buildDicepool(dicepoolParams []string) dice.Dicepool {

	dp := new(dice.Dicepool)

	for _, param := range dicepoolParams {

		dtype := param[len(param)-1:]
		count, _ := strconv.Atoi(param[0 : len(param)-1])

		switch dtype {
		case ABILITY_DTYPE:
			dp.Ability = dp.Ability + count
		case PROFICIENCY_DTYPE:
			dp.Proficiency = dp.Proficiency + count
		case BOOST_DTYPE:
			dp.Boost = dp.Boost + count
		case DIFFICULTY_DTYPE:
			dp.Difficulty = dp.Difficulty + count
		case CHALLENGE_DTYPE:
			dp.Challenge = dp.Challenge + count
		case SETBACK_DTYPE:
			dp.Setback = dp.Setback + count
		case FORCE_DTYPE:
			dp.Force = dp.Force + count
		case PERCETILE_DTYPE:
			dp.Percentile = true
			dp.PercentileOffset = count
		}

	}

	return *dp
}

func stripSpaces(input string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, input)
}
