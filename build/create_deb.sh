#!/bin/sh

APP_PATH=$GOPATH/src/bitbucket.org/HKiOnline/ndbot

echo "Changing to app directory."
cd $APP_PATH

echo "Creating Linux binary."
go install

echo "Removing old binary."
rm $APP_PATH/build/ndbot_1.0-1/usr/local/bin/ndbot

echo "Copying new binary to deb-template."
cp $GOPATH/bin/ndbot $APP_PATH/build/ndbot_1.0-1/usr/local/bin/

echo "Creating deb-archive."
dpkg-deb --build $APP_PATH/build/ndbot_1.0-1

echo "Uploading archive to Bitbucket."
cd $APP_PATH/build/
curl -u projektit@hkionline.net -X POST https://api.bitbucket.org/2.0/repositories/HKiOnline/ndbot/downloads -F files=@ndbot_1.0-1.deb

echo "Done."
