package main

import (
	"github.com/tucnak/telebot"
	"log"
	"os"
	"time"
)

var bot *telebot.Bot

func main() {

	log.Println("Starting Narrative Dice Telegram Bot (ndbot)")

	var configFilePath = os.Args[1]
	var configs = parseConfigurationFile(configFilePath)
	log.Println("Using API token " + configs.Apitoken)

	var err error
	bot, err = telebot.NewBot(configs.Apitoken)
	if err != nil {
		log.Fatalln(err)
	}

	bot.Messages = make(chan telebot.Message, 1000)

	go bot.Start(1 * time.Second)
	go messageProcessor()

	for {
		time.Sleep(time.Second * 4)
	}
}
