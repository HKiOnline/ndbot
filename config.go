package main

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	Apitoken string `json:"apitoken"`
}

func parseConfigurationFile(configFilePath string) Configuration {

	var configFile, err = os.Open(configFilePath)
	if err != nil {
		panic(err)
	}

	var configuration = Configuration{}

	var parser = json.NewDecoder(configFile)
	if err = parser.Decode(&configuration); err != nil {
		panic(err)
	}

	return configuration
}
